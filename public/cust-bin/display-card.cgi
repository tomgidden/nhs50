#!/usr/bin/perl --

# display-card.cgi
#  This uses HTML templates to construct a postcard for viewing
#
#  Contact: Tom Gidden, Brann Interactive,
#           +44 (0) 117 914 6257
#           tgidden@brann-interactive.com

# Clear-up notes:
#   The CGI uses temporary files within the $CARDS directory.
#   When a card is viewed, the file is renamed from 'unread...' to
#   'read...'.
#   Every night, a machine at Brann Interactive FTPs www.btwebworld.com
#   to upload date.gif, and to take a copy of the events database.
#   This script has been modified to remove any files in the $CARDS
#   directory, starting with 'read'.   I.e:
#
#     cd docs/postcards/inbox
#     prom
#     mdel read*



require 5.001;
require 'cgi-lib.pl';

# Base directory
$HOME = "/home/gid/projects/nhs50/cdrom/cust-bin/";

# Site URL (for eye-candy)
$MAINURL = "http://10.0.0.1/nhs50/";

# Self-referring URL
$ME = $MAINURL."cust-bin/display-card.cgi";

# URL of sender
$SEND_URL = $MAINURL."cust-bin/send-card.cgi";

# Location of stored card files
$CARDS = $HOME."inbox/";

# HTML Templates
$TEMPLATES = $HOME."templates/";

# Letter GIFs
$LETTERS = $HOME."letters/";

# Letter GIF URL
$LETURL = "letters/";

# URL (and path) of postcard images
$IMGURL = "images/";

# URL of structural images
$TOOLURL = "cardgfx/";

my %q;
ReadParse(\%q);

print "Content-type: text/html\n\n";
&displayCard(substr($q{'side'}, 0, 10), $q{'id'}, 500, 350);

################################################################################
# ddmmyy
sub ddmmyy
{
    my $sent = pop @_;
    my ($d, $mday, $mon, $year);

    ($d, $d, $d, $mday, $mon, $year, $d, $d, $d) = gmtime($sent+0);
    
    return (
            substr("00".$mday, -2).
            substr("00".($mon+1), -2).
            substr("00".$year, -2)
            );
}

################################################################################
# browserSniff: returns browser postfix
sub browserSniff
{
    my $HUA = $ENV{'HTTP_USER_AGENT'};
    
    if($HUA =~ /MSIE ([0-9])/)
    {
        return "msie4" if($1 > 4);
        return "msie$1";
    }
    elsif($HUA =~ /Mozilla\/([0-9])/)
    {
        return "netscape5" if($1 > 5);
        return "netscape$1";
    }
    else
    {
        return "other";
    }
}

################################################################################
# displayCard: loads the card file and displays it.  The core of the routine!
sub displayCard
{
    my ($side, $cardid, $mw, $mh) = @_;
    my %card;
    my $fn = "";

    ####################
    # Open card file
    if((($fn = $CARDS."unread-$cardid") && open (CARD, $fn)) ||
       (($fn = $CARDS."read-$cardid") && open (CARD, $fn)) ||
       (($fn = $CARDS."deletable-$cardid") && open (CARD, $fn)))
    {
        # Exclusive lock
        flock(CARD, 2); 

        my $key;
        my $val;

        open(CARDOUT, "> $CARDS$fn-temp");

        # Read in card details into %card
        while(<CARD>)
        {
            print CARDOUT $_;
            s/\r//g;
            chomp;
            ($key, $val) = split(/\t/);
            $card{$key} = $val;
        }

        close(CARDOUT);

        # Close lock
        flock(CARD, 8);
        close(CARD);

        rename($CARDS."$fn-temp", $CARDS.$fn);

        # Rename file for nightly external garbage collection
        if($fn =~ /\/unread/)
        {
            rename($fn, $CARDS."read-$cardid");
        }

        
        my $template;
        my %trans;
        
        if($side ne "back")
        {
            ####################
            # Open template
            $template = &loadFile($TEMPLATES."cardfront.htm");
            
            ####################
            # Load substitution table
            $trans{"title"} = "Your NHS: Postcard from ".$card{"fromname"};
            $trans{"cardgfx"} = $TOOLURL;
            $trans{"photo"} = $IMGURL.$card{'card'}."b.jpg";
            $trans{"maxwidth"} = $mw;
            $trans{"maxheight"} = $mh;
            $trans{"flip"} = "$ME?side=back&id=$cardid";
            $trans{"reply"} = "$SEND_URL";
            $trans{"MAINURL"} = $MAINURL;
            $trans{"toaddr"} = $card{"toaddr"};
            $trans{"toname"} = $card{"toname"};
            $trans{"fromaddr"} = $card{"fromaddr"};
            $trans{"fromname"} = $card{"fromname"};
        }
        else
        {
            ####################
            # Open template
            $template = &loadFile($TEMPLATES."cardback.htm");
            
            ####################
            # Load substitution table
            $trans{"title"} = "Your NHS: Postcard from ".$card{"fromname"};
            $trans{"cardgfx"} = $TOOLURL;
            $trans{"text"} = &createHandwriting($card{"message"}, $mw, $mh);

#&ddmmyy($card{'time'}).
            $trans{"stamp"} = "<table><tr><td valign=\"middle\" align=\"center\">".
                "<img src=\"$IMGURL"."franking/".
"230599".
".gif\">".
                    "</td><td valign=\"middle\" align=\"center\">".
                    "<img src=\"$IMGURL"."stamps/".$card{"stamp"}."b.gif\"></tr></table>";

            $trans{"addrline1"} = &createHandwriting("To: ".$card{"toname"}, $mw, $mh);
            $trans{"addrline2"} = &createHandwriting("From: ".$card{"fromname"}, $mw, $mh);
            $trans{"credits"} = $IMGURL.$card{'card'}."c.gif";
            $trans{"maxwidth"} = $mw;
            $trans{"maxheight"} = $mh;
            $trans{"flip"} = "$ME?side=front&id=$cardid";
            $trans{"MAINURL"} = $MAINURL;
            $trans{"reply"} = "$SEND_URL";
            $trans{"toaddr"} = $card{"toaddr"};
            $trans{"toname"} = $card{"toname"};
            $trans{"fromaddr"} = $card{"fromaddr"};
            $trans{"fromname"} = $card{"fromname"};
        }
        
        
        # Search -n- replace the template file.
        &modge($template, \%trans);
    }
    else
    {
        my $template;
        my %trans;

        ####################
        # Open template
        $template = &loadFile($TEMPLATES."postcard-error-view.htm");
        
        $trans{"error"} = "<p>Sorry, the postcard you have requested cannot be found.  Postcards are deleted a day after they are read.<br><br>To send a friend a card, visit <a href=\"http://www.nhs50.nhs.uk/\">the NHS50 website</a></p>";

        $trans{"MAINURL"} = $MAINURL;

        &modge($template, \%trans);
    }
}

################################################################################
# modge: fills in the blanks, and prints to stdout.
sub modge
{
    my ($template, $trans) = @_;
    
    my $key;

    # searches for <<blah>> and replaces with $$trans{'blah'}.
    $template =~ s/\<\<(\w+)\>\>/$$trans{$1}/g;
    print $template;
}

################################################################################

sub loadFile
{
    my ($fn) = @_;
    my $temp = "Not found";

    if((-f $fn) && open(TEMPLATE, $fn))
    {
        $temp = "";
        while(<TEMPLATE>)
        {
            $temp .= $_;
        }
        close(TEMPLATE);
    }

    return $temp;
}

################################################################################

sub createHandwriting
{
    my ($string, $mw, $mh) = @_;
    my %letters;
    my $out = "<nobr>";

    # Load the information table from the letters directory
    &loadLetters(\%letters);
    
    # Simple translations:
    # [ and < go to (
    $string=~ s/[\[\<]/\(/go;
    # ] and > go to )
    $string=~ s/[\]\>]/\)/go;
    # Remove anything else that looks suspect.
    $string=~ s/[^a-zA-Z0-9\(\)\,\.\!\?\-\=\&\*\"\'\`\:\;\��\$\ \^]//go;


#    $string = &wrapWords($string, $mw, \%letters);
    my $let;
    # For each letter
    foreach $let (split(//, $string))
    {
        my $ord = ord($let);
        if($let eq "^")
        {
            # ^ represents newline
            $out.= "<br>\n";
        }
        else
        {
            # insert the image tag for that letter
            $out.= "<img src=\"$LETURL".$letters{$ord}[0]."\" width=".$letters{$ord}[1]." height=".$letters{$ord}[2]." border=0>";

            # Okay, now when was the last time you saw this HTML tag used, hmmm?
            $out.= "</nobr>\n<nobr>" if(&isBreak($let));
        }
    }
    $out.= "</nobr>";
    
    return $out;
}

################################################################################
# loadLetters:  loads all letter files info into hash passed as parameter.
sub loadLetters
{
    my ($letptr) = @_;
    my @lets;
    my $let;

    if(opendir(DIR, $LETTERS))
    {
        @lets = grep /\.gif$/, readdir DIR;
        close DIR;

        # The ascii code, width and height is represented in the filename. 
        # This makes discovery a hell of a lot quicker.
        foreach $let (@lets)
        {
            if($let =~ /([0-9]+)\-([0-9]+)\-([0-9]+)/)
            {
                my $l = $1;
                my $w = $2;
                my $h = $3;
                $$letptr{$l} = [$let, $w, $h];
            }
        }
    }
}

################################################################################
sub isBreak
{
    my ($let) = @_;

    return ($let eq " ");
}

################################################################################
sub nextWord
{
    my ($string) = @_;

    my @words = split(/[ \^\n]/, $string);
    my $word = "";
    
    while($word !~ /\s/ && $#words)
    {
        $word .= pop @words;
    }

    return $word;
}

################################################################################
sub lenWord
{
    my ($word, $letptr) = @_;
    my $tot = 0;
    foreach $let (split //, $word)
    {
        $tot += $$letptr{ord($let)}[1];
        $tot = 0 if ($let eq '^');
    }
    return $tot;
}

################################################################################
sub wrapWords
{
    my ($string, $mw, $letptr) = @_;
    my $cw = 0;
    my @lines;
    my $cl = "";

    while(length($string)>0)
    {
        my $chr = substr($string, 0, 1);
        my $width = $$letptr{ord($chr)}[1];
        
        if($chr =~ /[\^\n]/)
        {
            push @lines, $cl;
            $string = substr($string, length($cl));
            $cl = "";
            $cw = 0;
        }
        else
        {
            $cl .= $chr;
            $cw += $width;
        }

        if($cw > $mw)
        {
            
        }
    }
}

