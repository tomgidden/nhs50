/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"


#ifndef _GID_FILTER_H_INCLUDED
Image *Filter_GaussianBlur(Image *in, double radius);


#define _GID_FILTER_H_INCLUDED
#endif
