/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
* Derived from ftstrpnm as distributed with FreeType                          *
*                                                                             *
******************************************************************************/

#include "image.h"

#ifndef _GID_TTFONT_H_INCLUDED
Image *CreateTextImage(char *txt);
void StartTTEngine();
void LoadTTFont(char *fontfile, int LCLptsize, int LCLdpi);
void CloseTTFont();
void FinishTTEngine();

#define _GID_TTFONT_H_INCLUDED
#endif

#define FONTPATH "/usr/local/ttfonts/"
