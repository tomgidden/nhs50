/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include "gd/gd.h"
#include "mix.h"


#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)

Image *Mix(Image *src1, Image *src2)
/* Mix src2 onto src1 using alpha -> dest image */
{
    return MixOff(src1, src2, 0, 0);
}

Image *MixOff(Image *src1, Image *src2, int ox, int oy)
/* Mix src2 onto src1 using alpha -> dest image */
{
    Image *dst;
    int x, y;
    RGB *rgb1, *rgb2;
    unsigned char al1, al2;
    int r, g, b, ceil;

    dst = CreateImage(
        MAX(src1->width, src2->width+ox), 
        MAX(src1->height, src2->height+oy)
        );

    
    for(x = 0; x<dst->width; x++)
        for(y = 0; y<dst->height; y++)
        {
            rgb1 = GetPixel(src1, x, y);
            rgb2 = GetPixel(src2, x-ox, y-oy);
            
            al1  = GetAlpha(src1, x, y);
            al2  = GetAlpha(src2, x-ox, y-oy);
            
            r = (double)((rgb1->r*(255-al2)) + (rgb2->r*al2)) / (double)255;
            g = (double)((rgb1->g*(255-al2)) + (rgb2->g*al2)) / (double)255;
            b = (double)((rgb1->b*(255-al2)) + (rgb2->b*al2)) / (double)255;
           
            ceil = al1 + al2; 
            ceil = (ceil>255)?255:ceil;

            PlotPixel(dst, x, y, r, g, b);
            PlotAlpha(dst, x, y, ceil);
        }

    return (dst);
}
