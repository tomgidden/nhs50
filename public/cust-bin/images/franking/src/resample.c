/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
*       NOTE: Right now, these routines only Minify, ie. scale down.          *
*       Magnification WILL coredump!                                          *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include "gd/gd.h"
#include "resample.h"

void Resample_Uniform(Image *dst, Image *src)
/* Resamples using uniform filter */
{
    int sw, sh, dw, dh;
    double scx, scy;
    int sx1, sy1, sx2, sy2;
    int dx, dy, sx, sy;
    int tr, tg, tb, ta;
    int count;
    int col;
    RGB *pixel;

    sw = src->width;
    sh = src->height;
    dw = dst->width;
    dh = dst->height;

    scx = (double)sw/(double)dw;
    scy = (double)sh/(double)dh;

    for(dx = 0; dx<dw; dx++)
        for(dy = 0; dy<dh; dy++)
        {
            sx1 = (int)(dx*scx);
            sx2 = (int)((dx+1)*scx);
            sy1 = (int)(dy*scy);
            sy2 = (int)((dy+1)*scy);
            
            tr = tg = tb = ta = 0;
            count = 0;

            for(sx = sx1; sx<sx2; sx++)
                for(sy = sy1; sy<sy2; sy++)
                {
                    pixel = GetPixel(src, sx, sy);
                    tr += pixel->r;
                    tg += pixel->g;
                    tb += pixel->b;
                    ta += GetAlpha(src, sx, sy);
                    
                    count ++;
                }

            PlotPixel(dst, dx, dy,
                      (unsigned char)((double)tr/(double)count),
                      (unsigned char)((double)tg/(double)count),
                      (unsigned char)((double)tb/(double)count));

            PlotAlpha(dst, dx, dy, (unsigned char)((double)ta/(double)count));
        }    
}

void Resample_Bartlett(Image *dst, Image *src)
/* Resamples using pseudo-Bartlett filter */
{
    int sw, sh, dw, dh;
    double scx, scy;
    int sx1, sy1, sx2, sy2, sxm, sym;
    int dx, dy, sx, sy;
    int tr, tg, tb, ta;
    int weight, weightdiv;
    RGB *pixel;

    sw = src->width;
    sh = src->height;
    dw = dst->width;
    dh = dst->height;

    scx = (double)sw/(double)dw;
    scy = (double)sh/(double)dh;

    for(dx = 0; dx<dw; dx++)
        for(dy = 0; dy<dh; dy++)
        {
            sx1 = (int)(dx*scx);
            sx2 = (int)((dx+1)*scx);
            sy1 = (int)(dy*scy);
            sy2 = (int)((dy+1)*scy);
            sxm = (sx1+sx2)>>1;
            sym = (sy1+sy2)>>1;

            tr = tg = tb = ta = 0;
            weightdiv = 0;
            
            for(sx = sx1; sx<sx2; sx++)
            {
                for(sy = sy1; sy<sy2; sy++)
                {
                    weight = (sx<sxm)?1+sx-sx1:sx2-sx;
                    weight*= (sy<sym)?1+sy-sy1:sy2-sy;

                    weightdiv += weight;
                    
                    pixel = GetPixel(src, sx, sy);
                    tr += pixel->r *weight;
                    tg += pixel->g *weight;
                    tb += pixel->b *weight;
                    ta += GetAlpha(src, sx, sy) *weight;
                }
            }

            PlotPixel(dst, dx, dy,
                      (unsigned char)((double)tr/(double)weightdiv),
                      (unsigned char)((double)tg/(double)weightdiv),
                      (unsigned char)((double)tb/(double)weightdiv));

            PlotAlpha(dst, dx, dy, 
                      (unsigned char)((double)ta/(double)weightdiv));
        }    
}


void Resample(Image *dst, Image *src, ResampleType filter)
/* Resamples the 'src' image into the 'dst' image. */
{
    switch (filter)
    {
    case RESAMPLE_UNIFORM:        Resample_Uniform(dst, src); break;
    case RESAMPLE_BARTLETT:       Resample_Bartlett(dst, src); break;
    }
}

