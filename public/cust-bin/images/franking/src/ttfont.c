/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
* Derived from ftstrpnm as distributed with FreeType                          *
*                                                                             *
******************************************************************************/

#include "freetype.h"
#include "image.h"
#include "ttfont.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TT_VALID(handle) ( ( handle ).z != NULL )


/* Global variables */

TT_Engine    engine;
TT_Face      face;
TT_Instance  instance;

TT_Raster_Map  bit;
TT_Raster_Map  small_bit;   /* used when font-smoothing is enabled */

int  pnm_width,   pnm_height;
int  pnm_x_shift, pnm_y_shift;

TT_Glyph  *glyphs = NULL;

static int  dpi;
static int  ptsize;

int  hinted;
int  smooth;
int  border;


/* raster map management */

static void  Init_Raster_Map( TT_Raster_Map *bit, int width, int height )
{
    bit->rows  = height;
    bit->width = ( width + 3 ) & -4;
    bit->flow  = TT_Flow_Down;

    if ( smooth )
    {
        bit->cols  = bit->width;
        bit->size  = bit->rows * bit->width;
    }
    else
    {
        bit->cols  = ( bit->width + 7 ) / 8;    /* convert to # of bytes     */
        bit->size  = bit->rows * bit->cols;     /* number of bytes in buffer */
    }

    bit->bitmap = (void *) malloc( bit->size );
}

static void  Done_Raster_Map( TT_Raster_Map *bit )
{
    free( bit->bitmap );
    bit->bitmap = NULL;
}

static void  Clear_Raster_Map( TT_Raster_Map *bit )
{
    memset( bit->bitmap, 0, bit->size );
}

static void  Blit_Or( TT_Raster_Map *dst, TT_Raster_Map *src,
                      int x_off, int y_off )
{
    int  x, y;
    int  x1, x2, y1, y2;
    char *s, *d;

    /* clipping */

    x1 = x_off < 0 ? -x_off : 0;
    y1 = y_off < 0 ? -y_off : 0;

    x2 = (int) dst->cols - x_off;
    if ( x2 > src->cols )  x2 = src->cols;

    y2 = (int) dst->rows - y_off;
    if ( y2 > src->rows )  y2 = src->rows;

    if ( x1 >= x2 )
        return;

    /* do the real work now */

    for ( y = y1; y < y2; ++y )
    {
        s = ( (char *) src->bitmap ) + y * src->cols + x1;
        d = ( (char *) dst->bitmap ) + ( y + y_off ) * dst->cols + x1 + x_off;

        for ( x = x1; x < x2; ++x )
            *d++ |= *s++;
    }
}

static void  ConvertToImage( TT_Raster_Map *bit, Image* im)
{
    char *bmap = (char *) bit->bitmap;

    int w, h;
    int x, y;
    RGB *pixel;
    int count = 0;
    int val = 0;

    w = bit->cols;
    h = bit->rows;
    
    for(y = 0; y<h; y++)
    {
        for(x = 0; x<w; x++)
        {
            bmap[count] = bmap[count] > 4 ? 4 : bmap[count];
            val = (bmap[count]*64);
            val = (val==256)?255:val;
            
            PlotPixel(im, x, y, val?255:0, val?255:0, val?255:0);
            PlotAlpha(im, x, y, val);
            
            count++;
        }
    }
}


static void  Dump_Raster_Map( TT_Raster_Map *bit, FILE *file )
{
    /* kudos for this code snippet go to Norman Walsh */

    char  *bmap; 
    int    i;

    bmap = (char *) bit->bitmap;

    if ( smooth )
    {
        fprintf( file, "P5\n%d %d\n4\n", pnm_width, pnm_height );
        for ( i = bit->size - 1; i >= 0; --i )
            bmap[i] = bmap[i] > 4 ? 0 : 4 - bmap[i];
        for ( i = pnm_height; i > 0; --i, bmap += bit->cols )
            fwrite( bmap, 1, pnm_width, file );
    }
    else
    {
        fprintf( file, "P4\n%d %d\n", pnm_width, pnm_height );
        for ( i = pnm_height; i > 0; --i, bmap += bit->cols )
            fwrite( bmap, 1, (pnm_width+7) / 8, file );
    }

    fflush( file );
}


/* glyph management */

static void  Load_Glyphs( char *txt, int txtlen )
{
    int         i, n, code, load_flags;
    int         num_glyphs = 0, no_cmap = 0;
    short       platform, encoding;
    TT_Error    error;
    TT_CharMap  char_map;

    /* First, look for a Unicode charmap */

    n = TT_Get_CharMap_Count( face );

    for ( i = 0; i < n; i++ )
    {
        TT_Get_CharMap_ID( face, i, &platform, &encoding );
        if ( (platform == 3 && encoding == 1 )  ||
             (platform == 0 && encoding == 0 ) )
        {
            TT_Get_CharMap( face, i, &char_map );
            break;
        }
    }

    if ( i == n )
    {
        TT_Face_Properties  properties;

        TT_Get_Face_Properties( face, &properties );

        no_cmap = 1;
        num_glyphs = properties.num_Glyphs;
    }


    /* Second, allocate the array */

    glyphs = (TT_Glyph *) malloc( 256 * sizeof( TT_Glyph ) );
    memset( glyphs, 0, 256 * sizeof( TT_Glyph ) );

    /* Finally, load the glyphs you need */

    load_flags = TTLOAD_SCALE_GLYPH;
    if ( hinted )
        load_flags |= TTLOAD_HINT_GLYPH;

    for ( i = 0; i < txtlen; ++i )
    {
        unsigned char  j = txt[i];

        if ( TT_VALID( glyphs[j] ) )
            continue;

        if ( no_cmap )
        {
            code = j - ' ' + 1;
            if ( code < 0 || code >= num_glyphs )
                code = 0;
        }
        else
        {
            code = TT_Char_Index( char_map, j );
            if ( code < 0 )
                code = 0;  /* FIXME! default code */
        }

        (void) (
            ( error = TT_New_Glyph( face, &glyphs[j] ) ) ||
            ( error = TT_Load_Glyph( instance, glyphs[j], code, load_flags ) )
            );
    }
}

static void  Done_Glyphs()
{
    int  i;

    if ( !glyphs )
        return;

    for ( i = 0; i < 256; ++i )
        TT_Done_Glyph( glyphs[i] );

    free( glyphs );

    glyphs = NULL;
}


/* face & instance management */

static void  Init_Face( const char *filename )
{
    TT_Error             error;
    char *newfn;
    
    newfn = (char*)malloc(strlen(FONTPATH) + strlen(filename));
    newfn = strcpy(newfn, FONTPATH);
    newfn = strcat(newfn, filename);
    
    
    /* load the typeface */

    error = TT_Open_Face( engine, newfn, &face );
    free(newfn);
 
    /* create and initialize instance */
 
    (void) (
        ( error = TT_New_Instance( face, &instance ) ) ||
        ( error = TT_Set_Instance_Resolutions( instance, 96, 96 ) ) ||
        ( error = TT_Set_Instance_CharSize( instance, ptsize * 64) )
        );
}

static void  Done_Face()
{
    TT_Done_Instance( instance );
    TT_Close_Face( face );
}


/* rasterization stuff */

static void  Init_Raster_Areas( const char *txt, int txtlen )
{
    int                  i, upm, ascent, descent;
    TT_Face_Properties   properties;
    TT_Instance_Metrics  imetrics;
    TT_Glyph_Metrics     gmetrics;

    /* allocate the large bitmap */

    TT_Get_Face_Properties( face, &properties );
    TT_Get_Instance_Metrics( instance, &imetrics );

    upm     = properties.header->Units_Per_EM;
    ascent  = ( properties.horizontal->Ascender  * imetrics.y_ppem ) / upm;
    descent = ( properties.horizontal->Descender * imetrics.y_ppem ) / upm;

    pnm_width   = 2 * border;
    pnm_height  = 2 * border + ascent - descent;

    for ( i = 0; i < txtlen; ++i )
    {
        unsigned char  j = txt[i];

        if ( !TT_VALID( glyphs[j] ) )
            continue;

        TT_Get_Glyph_Metrics( glyphs[j], &gmetrics );
        pnm_width += gmetrics.advance / 64;
    }

    /* ??? remove rsb of the last glyph here */

    Init_Raster_Map( &bit, pnm_width, pnm_height );
    Clear_Raster_Map( &bit );

    pnm_x_shift = border;
    pnm_y_shift = border - descent;

    /* allocate the small bitmap if you need it */

    if ( smooth )
        Init_Raster_Map( &small_bit, imetrics.x_ppem + 32, pnm_height );
}


static void  Done_Raster_Areas()
{
    Done_Raster_Map( &bit );
    if ( smooth )
        Done_Raster_Map( &small_bit );
}


static void  Render_Glyph( TT_Glyph glyph, int x_off, int y_off,
                           TT_Glyph_Metrics *gmetrics )
{
    if ( !smooth )
    {
        TT_Get_Glyph_Bitmap( glyph, &bit, x_off * 64, y_off * 64);
    }
    else
    {
        TT_F26Dot6  xmin, ymin, xmax, ymax;

        /* grid-fit the bounding box */

        xmin =  gmetrics->bbox.xMin & -64;
        ymin =  gmetrics->bbox.yMin & -64;
        xmax = (gmetrics->bbox.xMax + 63) & -64;
        ymax = (gmetrics->bbox.yMax + 63) & -64;

        /* now render the glyph in the small pixmap */
        /* and blit-or the resulting small pixmap into the biggest one */

        Clear_Raster_Map( &small_bit );
        TT_Get_Glyph_Pixmap( glyph, &small_bit, -xmin, -ymin );
        Blit_Or( &bit, &small_bit, xmin/64 + x_off, -ymin/64 - y_off );
    }
}

static void  Render_All_Glyphs( char *txt, int txtlen )
{
    int               i;
    TT_F26Dot6        x, y, adjx;
    TT_Glyph_Metrics  gmetrics;

    x = pnm_x_shift;
    y = pnm_y_shift;

    for ( i = 0; i < txtlen; i++ )
    {
        unsigned char  j = txt[i];

        if ( !TT_VALID( glyphs[j] ) )
            continue;

        TT_Get_Glyph_Metrics( glyphs[j], &gmetrics );

        adjx = x; /* ??? lsb */
        Render_Glyph( glyphs[j], adjx, y, &gmetrics );

        x += gmetrics.advance / 64;
    }
}

void StartTTEngine()
{
    TT_Init_FreeType(&engine);
}

void LoadTTFont(char *fontfile, int ptsize_arg, int dpi_arg)
{
    ptsize = ptsize_arg;
    dpi = dpi_arg;

    Init_Face(fontfile);
}

Image *CreateTextImage(char *txt)
{
    int txtlen;
    FILE *out;
    Image *im;
    
    smooth = 1;
    hinted = 0;

    txtlen = strlen( txt );
    
    Load_Glyphs( txt, txtlen );
    Init_Raster_Areas( txt, txtlen );
    
    Render_All_Glyphs( txt, txtlen );

    im = CreateImage(pnm_width, pnm_height);

    ConvertToImage(&bit, im);
    
    Done_Raster_Areas();
    Done_Glyphs();
     
    return (im);
}

void CloseTTFont()
{
    Done_Face();
}

void FinishTTEngine()
{
    TT_Done_FreeType(engine);
}
