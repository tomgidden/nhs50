/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"


#ifndef _GID_RESAMPLE_H_INCLUDED
typedef enum
{
    RESAMPLE_UNIFORM=1,
    RESAMPLE_BARTLETT=2
} ResampleType;

void Resample(Image *dst, Image *src, ResampleType filter);
#define _GID_RESAMPLE_H_INCLUDED
#endif
