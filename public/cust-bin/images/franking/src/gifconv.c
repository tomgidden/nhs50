/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"
#include "gifconv.h"
#include <math.h>
#include <string.h>


unsigned char bound(int x)
{
    return (unsigned char)((x<0)?0:((x>255)?255:x));
}

unsigned char CastrateColor(unsigned char in)
/* This rounds the given value to the nearest multiple of 51 from 0 to 255 */
{
   
    return bound(((in/51))*51);
}



gdImagePtr ConvertImageToGd_fcfs(Image* im)
/* converts an Image to a gd image using First-Come-First-Served color alloc */
{
    gdImagePtr gd;
    int x, y, count = 0;
    RGB *rgb;
    unsigned char alpha;

    gd = gdImageCreate(im->width, im->height);


    for(y = 0; y<im->height; y++)
    {
        for(x = 0; x<im->width; x++)
        {
            rgb = GetPixel(im, x, y);
            alpha = GetAlpha(im, x, y);
            gdImageSetPixel(gd, x, y, AllocateOrFindColor(gd, 
                                                          Alpherize(rgb->r, alpha),
                                                          Alpherize(rgb->g, alpha),
                                                          Alpherize(rgb->b, alpha)));
                                     
            count ++;
        }
    }
    return gd;
}


gdImagePtr ConvertImageToGd_safety_trad(Image* im)
/* converts an Image to a gd image using the unified web safety palette */
{
    gdImagePtr gd;
    int x, y;
    RGB *rgb;
    int r,g,b;
    unsigned char alpha;
    
    gd = gdImageCreate(im->width, im->height);

    for(y = 0; y<im->height; y++)
    {
        for(x = 0; x<im->width; x++)
        {
            rgb = GetPixel(im, x, y);
            alpha = GetAlpha(im, x, y);
            gdImageSetPixel(gd, x, y, AllocateOrFindColor(gd, 
                                                          CastrateColor(Alpherize(rgb->r, alpha)),
                                                          CastrateColor(Alpherize(rgb->g, alpha)),
                                                          CastrateColor(Alpherize(rgb->b, alpha))));
        }
    }
    return gd;
}




gdImagePtr ConvertImageToGd_safety_adapt(Image* im)
/* converts an Image to a gd image using the unified web safety palette and Floyd-Steinberg Error Diffusion*/
{
    gdImagePtr gd;
    Image *temp;
    int x, y, xc;
    RGB *rgb;
    int r,g,b;
    unsigned char alpha;
    int Er, Eg, Eb;
    unsigned char Kr, Kg, Kb;
    int dir; 

    temp = CopyImage(im);


    /* Flatten Image */
    for(y = 0; y<temp->height; y++)
    {
        for(x = 0; x<temp->width; x++)
        {
            rgb = GetPixel(temp, x, y);
            alpha = GetAlpha(temp, x, y);
            PlotPixel(temp, x, y, 
                      Alpherize(rgb->r, alpha),
                      Alpherize(rgb->g, alpha),
                      Alpherize(rgb->b, alpha));
            PlotAlpha(temp, x, y, 255);
        }
    }
    

    /* FSI (Floyd Steinberg Error Diffusion [integer]) */

    dir = 1; /* Direction for weave */

    for(y = 0; y<temp->height; y++)
    {
        for(xc = 0; xc<temp->width; xc++)
        {
            if(dir == 1) 
            { dir = -1; x = temp->width-1-xc; } 
            else
            { dir = 1; x = xc; } 
                
            rgb = GetPixel(temp, x, y);

            r = rgb->r;
            g = rgb->g;
            b = rgb->b;
            Kr = CastrateColor(r);
            Kg = CastrateColor(g);
            Kb = CastrateColor(b);
            Er = r-Kr;
            Eg = g-Kg;
            Eb = b-Kb;
            
            PlotPixel(temp, x, y, 
                      Kr, 
                      Kg, 
                      Kb);
            

            rgb = GetPixel(temp, x+dir, y);
            PlotPixel(temp, x+dir, y, 
                      bound(rgb->r + (unsigned char)(7.0/16.0*(double)Er)),
                      bound(rgb->g + (unsigned char)(7.0/16.0*(double)Eg)),
                      bound(rgb->b + (unsigned char)(7.0/16.0*(double)Eb)));

            rgb = GetPixel(temp, x-dir, y+1);
            PlotPixel(temp, x-dir, y+1,
                      bound(rgb->r + (unsigned char)(3.0/16.0*(double)Er)),
                      bound(rgb->g + (unsigned char)(3.0/16.0*(double)Eg)),
                      bound(rgb->b + (unsigned char)(3.0/16.0*(double)Eb)));

            rgb = GetPixel(temp, x, y+1);
            PlotPixel(temp, x, y+1,
                      bound(rgb->r + (unsigned char)(5.0/16.0*(double)Er)),
                      bound(rgb->g + (unsigned char)(5.0/16.0*(double)Eg)),
                      bound(rgb->b + (unsigned char)(5.0/16.0*(double)Eb)));
                   
            rgb = GetPixel(temp, x+dir, y+1);
            PlotPixel(temp, x+dir, y+1,
                      bound(rgb->r + (unsigned char)(1.0/16.0*(double)Er)),
                      bound(rgb->g + (unsigned char)(1.0/16.0*(double)Eg)),
                      bound(rgb->b + (unsigned char)(1.0/16.0*(double)Eb)));
                
        }
    }

    gd = gdImageCreate(temp->width, temp->height);

    for(y = 0; y<temp->height; y++)
    {
        for(x = 0; x<temp->width; x++)
        {
            rgb = GetPixel(temp, x, y);

            gdImageSetPixel(gd, x, y, AllocateOrFindColor(gd, 
                                                          rgb->r,
                                                          rgb->g,
                                                          rgb->b));

        }
    }

    DestroyImage(temp);

    return gd;
}

gdImagePtr ConvertImageToGd(Image* im, PalettingMethod meth)
/* converts an Image to a gd image. */
{
    switch (meth)
    {
    case PALETTE_SAFETY_TRAD:                   return ConvertImageToGd_safety_trad(im); break;
    case PALETTE_SAFETY_ADAPT:                  return ConvertImageToGd_safety_adapt(im); break;
    default:
    case PALETTE_FIRST_COME_FIRST_SERVED:       return ConvertImageToGd_fcfs(im); break;
    }
}

