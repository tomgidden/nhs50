/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"
#include <string.h>

unsigned char Alpherize(unsigned char x, unsigned char a)
/* Does an alpha-fade */
{
    return((unsigned char)((double)(x*a)/(double)255));

}

int AllocateOrFindColor(gdImagePtr gd, int r, int g, int b)
/* Finds an exact match, allocates, or worst case, finds closest. */
{
    int col = gdImageColorExact(gd, r, g, b);
    if(col == -1) col = gdImageColorAllocate(gd, r, g, b);
    if(col == -1) col = gdImageColorClosest(gd, r, g, b);

    return col;
}



void PlotPixel(Image *im, int x, int y, unsigned char r, unsigned char g, unsigned char b)
/* tries to plot a pixel */
{
    int off = x+(y*im->width);

    if((x>=0) && (y>=0) && (x<=im->width) && (y<=im->height))
    {
        RGB *rgb = &(im->image[off]);
        
        rgb->r = r;
        rgb->g = g;
        rgb->b = b;
    }
}

void PlotAlpha(Image *im, int x, int y, unsigned char a)
/* tries to plot an alpha-pixel */
{
    int off = x+(y*im->width);

    if((x>=0) && (y>=0) && (x<=im->width) && (y<=im->height))
    {
        im->alpha[off] = a;
    }
}

RGB *GetPixel(Image *im, int x, int y)
/* tries to get a pixel */
{
    int off = x+(y*im->width);
    
    if((x<0) || (y<0) || (x>=im->width) || (y>=im->height)) return (&(im->image[0]));

    return (&(im->image[off]));
}

unsigned char GetAlpha(Image *im, int x, int y)
/* tries to get the alpha pixel */
{
    int off = x+(y*im->width);

    if((x<0) || (y<0) || (x>=im->width) || (y>=im->height)) return 0;

    return (im->alpha[off]);
}

Image* CreateImage(int width, int height)
/* Creates an image, with no particular contents */
{
    Image *im;
    RGB *img;
    int x;

    im = (Image *)malloc(sizeof(Image));
    im->image = (RGB *)malloc(width*height*sizeof(RGB));
    im->alpha = (unsigned char *)malloc(width*height*sizeof(unsigned char));
    im->flags = 0;

    /* clear to white */
    memset(im->image, 255, width*height*sizeof(RGB));
    memset(im->alpha, 255, width*height*sizeof(unsigned char));

    im->width = width;
    im->height = height;

    if( (im->alpha == NULL) ||
        (im->image == NULL) )
    {
        printf("Argh!\n");
        exit(0);
    }

    return (im);
}

Image* CopyImage(Image *orig)
/* Copies an image identically */
{
    Image *im;
    int width, height;

    width = orig->width;
    height= orig->height;

    im = (Image *)malloc(sizeof(Image));
    im->image = (RGB *)malloc(width*height*sizeof(RGB));
    im->alpha = (unsigned char *)malloc(width*height*sizeof(unsigned char));
    im->flags = orig->flags;

    bcopy(orig->image, im->image, width*height*sizeof(RGB));
    bcopy(orig->alpha, im->alpha, width*height*sizeof(unsigned char));
    
    im->width = width;
    im->height = height;

    if( (im->alpha == NULL) ||
        (im->image == NULL) )
    {
        printf("Argh!\n");
        exit(0);
    }

    return (im);
}

void DestroyImage(Image* im)
/* Deallocates memory */
{
    free(im->image);
    free(im->alpha);
    free(im);
}

Image* ConvertGdToImage(gdImagePtr gd)
/* Creates an Image using a gd image as a source.   Arses around with memory */
{
    Image* im;
    int x, y;
    int count = 0;
    int col;

    im = CreateImage(gdImageSX(gd), gdImageSY(gd));

    for(x = 0; x<im->width; x++)
        for(y = 0; y<im->height; y++)
        {
            col = gdImageGetPixel(gd, x, y);
            PlotPixel(im, x, y,  
                      gdImageRed(gd, col), 
                      gdImageGreen(gd, col),
                      gdImageBlue(gd, col));
            PlotAlpha(im, x, y, 255);
        }
    
    return (im);
}

void FillImageWithColor(Image *im, RGB *col)
{
    int off;

    for(off = 0; off<(im->width*im->height); off++)
    {
        RGB *rgb = &(im->image[off]);

        rgb->r = col->r;
        rgb->g = col->g;
        rgb->b = col->b;

        if(!(im->flags | IMAGE_PRESERVE_TRANSPARENCY))
            im->alpha[off] = 255;
    }
}

void FillImageWithColorRGB(Image *im, int r, int g, int b)
{
    int off;

    for(off = 0; off<(im->width*im->height); off++)
    {
        RGB *rgb = &(im->image[off]);

        rgb->r = r;
        rgb->g = g;
        rgb->b = b;

        if(!(im->flags | IMAGE_PRESERVE_TRANSPARENCY))
            im->alpha[off] = 255;
    }
}

void FillImageWithBlack(Image *im)
{
    bzero(im->image, im->width*im->height*sizeof(RGB));

    if(!(im->flags | IMAGE_PRESERVE_TRANSPARENCY))
        memset(im->alpha, 255, im->width*im->height);
}

void FillImageWithWhite(Image *im)
{
    memset(im->image, 255, im->width*im->height*sizeof(RGB));

    if(!(im->flags | IMAGE_PRESERVE_TRANSPARENCY))
        memset(im->alpha, 255, im->width*im->height);
}

void InvertImage(Image *im)
{
    int off;

    for(off = 0; off<(im->width*im->height); off++)
    {
        RGB *rgb = &(im->image[off]);

        rgb->r = !rgb->r;
        rgb->g = !rgb->g;
        rgb->b = !rgb->b;
    }
}

void InvertAlpha(Image *im)
{
    int off;

    for(off = 0; off<(im->width*im->height); off++)
    {
        im->alpha[off] = !im->alpha[off];
    }
}


Image *LoadGifAsImage(char *filename)
{
    FILE *inp;
    Image *blah;

    inp = fopen(filename, "rb");
    
    blah =  ConvertGdToImage(gdImageCreateFromGif(inp));

    fclose(inp);
    
    return(blah);
}
