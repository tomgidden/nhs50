#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include "gd/gd.h"
#include "image.h"
#include "resample.h"
#include "mix.h"
#include "ttfont.h"

extern void initialiseProcess();
extern void finishProcess();
extern gdImagePtr processImage(char *text);


#if 0

void processText(char *str, char *filename)
{
    gdImagePtr gd;
    FILE *fout;

    if(fout = fopen(filename, "wb"))
    {
        gd = processImage(str);
        gdImageGif(gd, fout);
        fclose(fout);
        gdImageDestroy(gd);
    }
}


void processFile(int fd)
{
    FILE *ffd;
    FILE *fout;
    char *line;
    size_t len;
    char *filename;
    char *text;
    gdImagePtr gd;


    if(ffd = fdopen(fd, "r"))
    {
        while(!feof(ffd))
        {
            if(line = fgetln(ffd, &len))
            {
                line[len-1] = '\0';

                filename = strsep(&line, "#");
                if(strlen(filename))
                {
                    text = strsep(&line, "#");
                    
                    processText(text, filename);
                }
            }
        }
        
        if(fd != fileno(stdin)) fclose(ffd);
    }

}

int main(int argc, char *argv[])
{
    int fd;

    initialiseProcess();

    if(argc>1)
    {
        while (*argv++)
        {
            if(fd = open(*argv, O_RDONLY, 0))
            {
                processFile(fd);
                (void)close(fd);
            }
        }
    }
    else
    {
        processFile(fileno(stdin));
    }

    finishProcess();
}

#else


void processText(char *str)
{
    gdImagePtr gd;

    gd = processImage(str);
    gdImageGif(gd, fdopen(STDOUT_FILENO, "a"));
    gdImageDestroy(gd);
}

int main(int argc, char *argv[])
{
    initialiseProcess();
    processText(*++argv);
    finishProcess();
}

#endif



