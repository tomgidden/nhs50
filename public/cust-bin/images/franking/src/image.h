/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"

#ifndef _GID_IMAGE_H_INCLUDED

typedef enum
{
    IMAGE_PRESERVE_TRANSPARENCY = 1
} ImageFlags;

typedef struct
{
    unsigned char r, g, b;
} RGB;

typedef struct 
{
    unsigned int width, height;
    RGB *image;
    unsigned char *alpha;
    long flags;
} Image;

typedef gdImagePtr Gif;

void PlotPixel(Image *im, int x, int y, unsigned char r, unsigned char g, unsigned char b);
void PlotAlpha(Image *im, int x, int y, unsigned char a);

RGB  *GetPixel(Image *im, int x, int y);
unsigned char GetAlpha(Image *im, int x, int y);



int AllocateOrFindColor(gdImagePtr gd, int r, int g, int b);
Image* CopyImage(Image *orig);
Image* CreateImage(int width, int height);
void DestroyImage(Image* im);
Image* ConvertGdToImage(gdImagePtr gd);
void FillImageWithBlack(Image *im);
void FillImageWithWhite(Image *im);
void FillImageWithColor(Image *im, RGB *col);
void FillImageWithColorRGB(Image *im, int r, int g, int b);
void InvertImage(Image *im);
void InvertAlpha(Image *im);
Image *LoadGifAsImage(char *filename);

#define _GID_IMAGE_H_INCLUDED
#endif
