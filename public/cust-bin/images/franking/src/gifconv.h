/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"


#ifndef _GID_GIFCONV_H_INCLUDED

typedef enum
{
    PALETTE_FIRST_COME_FIRST_SERVED = 1,
    PALETTE_SAFETY_TRAD = 2,
    PALETTE_SAFETY_ADAPT = 3
} PalettingMethod;


gdImagePtr ConvertImageToGd(Image* im, PalettingMethod meth);


#define _GID_GIFCONV_H_INCLUDED
#endif



