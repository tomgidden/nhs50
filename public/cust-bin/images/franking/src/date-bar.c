#include "gd/gd.h"
#include "image.h"
#include "gifconv.h"
#include "resample.h"
#include "mix.h"
#include "ttfont.h"
#include "filter.h"
#include <stdio.h>

/********************************************************************************/

/* Define your images and GIFs here! 
 *
 * Defining an image doesn't describe the size or shape of the image --
 * it just notifies the system that you'll be using an image with this
 * internal name.
 */

Image *bg;
Image *im;
Image *im1, *im2;
Image *small;
Image *out;
Gif gd;

/********************************************************************************/

void initialiseProcess()

/* This is the routine that you do BEFORE a batch..
 *
 * For example, if you're generating 1000 images using the same background image,
 * only load that background image once! 
 * Also, it's handy to start the Font Engine and load the font here.
 */    

{
    StartTTEngine();
    LoadTTFont("gnc_____.ttf", 8*4, 288);

    /*    bg = LoadGifAsImage("src.gif"); */
}

/********************************************************************************/

void finishProcess()

/* This is the routine that you do AFTER the batch..
 *
 * This should typically clear up the memory, by shutting down the font engine
 * and destroying any images you've used.
 */

{
    CloseTTFont();
    FinishTTEngine();
}

/********************************************************************************/

gdImagePtr processImage(char *text)

/* This is the batch action, ie. the recipe to make one GIF, when given a piece
 * of text to work on.
 * 
 * Return a GIF at the end, probably with a ConvertImageToGd() command, and it'll
 * be happy.  As this might be run hundreds of times, for ****'s sake, deallocate
 * your memory!
 */

{
    int ox, oy;

    bg = LoadGifAsImage("src.gif");
    im = CreateTextImage(text);
    small = CreateImage(im->width>>2, im->height>>2);
    
    ox = (bg->width - small->width)>>1;
    oy = ((bg->height - small->height)>>1)+1;
    
    im->flags |= IMAGE_PRESERVE_TRANSPARENCY;
    FillImageWithColorRGB(im, 153,51,51);
    
    Resample(small, im, RESAMPLE_BARTLETT);
    DestroyImage(im);
    
    im = MixOff(bg, small, ox, oy);
    im1 = MixOff(im, small, ox, oy);
    
    gd = ConvertImageToGd(im1, PALETTE_FIRST_COME_FIRST_SERVED); 
    gdImageColorTransparent(gd, gdImageColorExact(gd, 192,192,192));
    
    
    DestroyImage(bg);
    DestroyImage(im);
    DestroyImage(im1);
    DestroyImage(small);
 
    return(gd);
}

/********************************************************************************/

