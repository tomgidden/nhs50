/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "filter.h"
#include "image.h"
#include "math.h"

Image *Filter_GaussianBlur(Image *in, double radius)
/* Gaussian Blurs (ishy) the image into a new image */
{
    Image *out;
    double *convol;
    double PI;
    double rad2;
    double total, totmin;
    double f;
    int x, y;
    int j, k;
    unsigned char alpha;
    RGB *rgb;
    int r, g, b;
    int a;
    double weight;

    rad2 = ceil(radius) * 3.0;

    convol = (double *)malloc(4*rad2*rad2*sizeof(double));
    
    if(convol == NULL) return in;

    PI = atan2(1,1)*4.0;

    out = CreateImage(in->width, in->height);
    
    total = 0;

    for(j = 1-rad2; j<rad2; j++)
    {
        for(k = 1-rad2; k<rad2; k++)
        {
            f = 2*PI*radius * exp((-(j*j + k*k))/(2*radius*radius));
            *(convol+(int)(rad2+j+((k+rad2)*2*rad2))) = f;
            
            total += f;
        }
    }

    for(x = 0; x<in->width; x++)
        for(y = 0; y<in->height; y++)
        {
            r = g = b = a = 0;
            totmin = total;

            for(j = 1-rad2; j<rad2; j++)
                for(k = 1-rad2; k<rad2; k++)
                {
                    weight = *(convol+(int)(rad2+j+((k+rad2)*2*rad2)));
                    

                    if(x+j>=0 && x+j<in->width &&
                       y+k>=0 && y+k<in->height)
                    {
                        rgb = GetPixel(in, x+j, y+k);
                        alpha = GetAlpha(in, x+j, y+k);
                        
                        r += (int)((double)rgb->r*weight)+.5;
                        g += (int)((double)rgb->g*weight)+.5;
                        b += (int)((double)rgb->b*weight)+.5;
                        a += (int)((double)alpha*weight)+.5;
                    }
                    else
                    {
                        totmin -= weight;
                    }
                }

            r /= totmin;
            g /= totmin;
            b /= totmin;
            a /= totmin;

            PlotPixel(out, x, y, r, g, b);
            PlotAlpha(out, x, y, a);
        }

    free(convol);

    return (out);
}




/*  Gaussian Filter generator in Perl :
$pi = atan2(1,1)*4;
$theta = 1.5;

$str = " -+=#*O\@1234567890qwertyuiop";

for($theta=0.1; $theta<2; $theta += .1)
{
    $radius = int($theta)*3+3;

    $count = 0;

    for($j=-$radius+1; $j<$radius; $j++)
    {
        for($k=-$radius+1; $k<$radius; $k++)
        {
            $f = (2*$pi*$theta) * exp((-($j*$j + $k*$k))/(2*$theta*$theta));
            
            print substr($str, int($f), 1)." ";
            
            $count+=int($f);
        }
        
        print "\n";
    }

    print "$count\n\n";
}
*/   
