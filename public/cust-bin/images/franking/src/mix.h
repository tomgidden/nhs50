/******************************************************************************
* AGG System                                                                  *
* by Tom Gidden                                                               *
*                                                                             *
* Uses FreeType and GD libraries.. refer to those distributions for licencing *
*                                                                             *
******************************************************************************/

#include "gd/gd.h"
#include "image.h"

#ifndef _GID_MIX_H_INCLUDED
Image *Mix(Image *src1, Image *src2);
Image *MixOff(Image *src1, Image *src2, int ox, int oy);
#define _GID_MIX_H_INCLUDED
#endif
