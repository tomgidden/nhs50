#include "gd/gd.h"
#include "image.h"
#include "gifconv.h"
#include "resample.h"
#include "mix.h"
#include "ttfont.h"
#include "filter.h"
#include <stdio.h>
#include <unistd.h>

/********************************************************************************/

/* Define your images and GIFs here! 
 *
 * Defining an image doesn't describe the size or shape of the image --
 * it just notifies the system that you'll be using an image with this
 * internal name.
 */

Image *bg1, *bg2;
Image *im;
Image *im1, *im2;
Image *small, *arrowl, *arrowr;
Image *out;
Gif gd;

/********************************************************************************/

void initialiseProcess()

/* This is the routine that you do BEFORE a batch..
 *
 * For example, if you're generating 1000 images using the same background image,
 * only load that background image once! 
 * Also, it's handy to start the Font Engine and load the font here.
 */    

{
    StartTTEngine();
    LoadTTFont("gnb_____.ttf", 8.8*4, 288);

    bg1 = LoadGifAsImage("lozenge-l.gif");
    bg2 = LoadGifAsImage("lozenge-r.gif");

    arrowl = CreateImage(6, 7);
    FillImageWithColorRGB(arrowl, 102, 0, 0);
    PlotAlpha(arrowl, 0, 0, 0);
    PlotAlpha(arrowl, 0, 1, 0);
    PlotAlpha(arrowl, 1, 0, 0);
    PlotAlpha(arrowl, 1, 1, 0);
    PlotAlpha(arrowl, 0, 5, 0);
    PlotAlpha(arrowl, 0, 6, 0);
    PlotAlpha(arrowl, 1, 5, 0);
    PlotAlpha(arrowl, 1, 6, 0);
    PlotAlpha(arrowl, 3, 0, 0);
    PlotAlpha(arrowl, 4, 0, 0);
    PlotAlpha(arrowl, 5, 0, 0);
    PlotAlpha(arrowl, 4, 1, 0);
    PlotAlpha(arrowl, 5, 1, 0);
    PlotAlpha(arrowl, 5, 2, 0);
    PlotAlpha(arrowl, 5, 4, 0);
    PlotAlpha(arrowl, 4, 5, 0);
    PlotAlpha(arrowl, 5, 5, 0);
    PlotAlpha(arrowl, 3, 6, 0);
    PlotAlpha(arrowl, 4, 6, 0);
    PlotAlpha(arrowl, 5, 6, 0);

    arrowr = CreateImage(6, 7);
    FillImageWithColorRGB(arrowr, 102, 0, 0);
    PlotAlpha(arrowr, 5-0, 0, 0);
    PlotAlpha(arrowr, 5-0, 1, 0);
    PlotAlpha(arrowr, 5-1, 0, 0);
    PlotAlpha(arrowr, 5-1, 1, 0);
    PlotAlpha(arrowr, 5-0, 5, 0);
    PlotAlpha(arrowr, 5-0, 6, 0);
    PlotAlpha(arrowr, 5-1, 5, 0);
    PlotAlpha(arrowr, 5-1, 6, 0);
    PlotAlpha(arrowr, 5-3, 0, 0);
    PlotAlpha(arrowr, 5-4, 0, 0);
    PlotAlpha(arrowr, 5-5, 0, 0);
    PlotAlpha(arrowr, 5-4, 1, 0);
    PlotAlpha(arrowr, 5-5, 1, 0);
    PlotAlpha(arrowr, 5-5, 2, 0);
    PlotAlpha(arrowr, 5-5, 4, 0);
    PlotAlpha(arrowr, 5-4, 5, 0);
    PlotAlpha(arrowr, 5-5, 5, 0);
    PlotAlpha(arrowr, 5-3, 6, 0);
    PlotAlpha(arrowr, 5-4, 6, 0);
    PlotAlpha(arrowr, 5-5, 6, 0);

}

/********************************************************************************/

void finishProcess()

/* This is the routine that you do AFTER the batch..
 *
 * This should typically clear up the memory, by shutting down the font engine
 * and destroying any images you've used.
 */

{
    CloseTTFont();
    FinishTTEngine();
}

/********************************************************************************/

gdImagePtr processImage(char *text)

/* This is the batch action, ie. the recipe to make one GIF, when given a piece
 * of text to work on.
 * 
 * Return a GIF at the end, probably with a ConvertImageToGd() command, and it'll
 * be happy.  As this might be run hundreds of times, for ****'s sake, deallocate
 * your memory!
 */

{
    int ox, oy;
    int x, y;
    
    Image *loz;
    Image *temp;
    
    im = CreateTextImage(text);
    small = CreateImage(im->width>>2, im->height>>2);
    im->flags |= IMAGE_PRESERVE_TRANSPARENCY;
    FillImageWithColorRGB(im, 102,0,0);
    Resample(small, im, RESAMPLE_BARTLETT);
    DestroyImage(im);
    
    loz = CreateImage(small->width + 18, bg1->height);
    temp = MixOff(loz, bg1, 0, 0);
    DestroyImage(loz);
    loz = temp;

    for(x=(bg1->width); x<=((loz->width) - (bg2->width)); x++)
      {
        temp = MixOff(loz, bg2, x, 0);
        DestroyImage(loz);
        loz = temp;
      }
    
    ox = (loz->width - small->width)>>1;
    oy = ((loz->height - small->height)>>1)+1;
    

    im = MixOff(loz, small, ox, oy);
    DestroyImage(loz);
    loz = im;
    /*    im = MixOff(loz, arrowl, ox + small->width + 2, (loz->height - arrowl->height)>>1); */

    gd = ConvertImageToGd(im, PALETTE_FIRST_COME_FIRST_SERVED); 
    gdImageColorTransparent(gd, gdImageColorExact(gd, 192,192,192));
    
    /*    DestroyImage(loz); */
    DestroyImage(im);
    DestroyImage(small);
 
    return(gd);
    
}

/********************************************************************************/

