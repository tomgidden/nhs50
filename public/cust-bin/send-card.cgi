#!/usr/bin/perl --

# send-card.cgi
#  This uses HTML templates to step through the process of sending a 
#  postcard
#  Contact: Tom Gidden, Brann Interactive,
#           +44 (0) 117 914 6257
#           tgidden@brann-interactive.com

require 5.001;
require 'cgi-lib.pl';

# Real Home
$REALHOME = "/home/gid/projects/nhs50/cdrom/cust-bin/";

# Base directory
$HOME = $REALHOME;

# Site URL (for eye-candy)
$MAINURL = "http://10.0.0.1/nhs50/";

# Self-referring URL
$ME = $MAINURL."cust-bin/send-card.cgi";

# URL of displayer
$DISPLAY_URL = $MAINURL."cust-bin/display-card.cgi";

# Location of stored card files
$CARDS = $HOME."inbox/";

# HTML Templates
$TEMPLATES = $HOME."templates/";

# Letter GIFs
# $LETTERS = $HOME."letters/";

# Letter GIF URL
# $LETURL = "letters/";

# URL (and path) of postcard images
$IMAGES = $HOME."images/";
$IMGURL = "images/";

# URL of structural images
# $TOOLURL = "cardgfx/";

# Maximum lengths
$MAXMESS = 128;
$MAXTA = 64;
$MAXTN = 32;
$MAXFA = 64;
$MAXFN = 32;
$MAXCARD = 3;
my %q;
ReadParse(\%q);


print "Content-type: text/html\n\n";

# Compose stage.
if($q{'stage_compose.x'})
{ &cardCompose; }
# Stamp selection stage (if card details from 'Compose' is okay)
elsif($q{'stage_stamp.x'})
{ &cardStamp if(&cardChecksettings); }
# Send stage
elsif($q{'stage_send.x'})
{ &cardSend; }
# Blowup stage
elsif(&getBlowup)
{ $q{'card'} = &getBlowup; &cardBlowup; }
# Otherwise start the process
else
{ &cardSelect; }

################################################################################
# sender: sends mail in line with BT Webworld's wrapped sendmail
sub sender
{
    my ($sender, $to, $subject, $message) = @_;

    my $EXTENSION = 0;
    my $QUEUE_DIR = "/var/spool/mqueue";
    my $MESSAGE_FILE;

    do {
        $EXTENSION ++;
        $MESSAGE_FILE = "$QUEUE_DIR/$<.$$.$EXTENSION";
    } until ((! -e $MESSAGE_FILE) & (! -e "$QUEUE_DIR/mail.$<.$$.$EXTENSION"));

    open(FILE, ">$MESSAGE_FILE") or return 6;
    print FILE "From: $sender\nTo: $to\nSubject: $subject\n\n$message\n";
    close FILE;

    rename( $MESSAGE_FILE, "$QUEUE_DIR/mail.$<.$$.$EXTENSION" )
        or return 7;

    return 0;
}

################################################################################
# hacknChop: trims up the string and modges horrible characters out.
sub hacknChop
{
    my ($string, $max) = @_;

    $string =~ s/[\n\t\r]/\^/g;
    $string =~ s/\s+/ /g;
    return substr($string, 0, $max);
}

################################################################################
# hacknChop2: trims up the string and modges horrible characters out. INCLUDING
# other weird chars
sub hacknChop2
{
    my ($string, $max) = @_;

    $string =~ s/[\n\t\r]/\^/g;
    $string =~ s/\s+/ /g;
    $string =~ s/\"/\'/g;
    return substr($string, 0, $max);
}

################################################################################
# cardSend:  this saves the message in the inbox directory
sub cardSend
{
    my %trans;
    my $template = &loadFile($TEMPLATES."postcard-sent.htm");
    my $ret = 0;
    my $mess = "";
    my $id = "$$-".(time);
    my $fn = $CARDS."unread-$id";
    
    $q{'message'} =~ s/[\r\n]+/^ /g;
    $q{'message'} =~ s/[\t]+/ /g;

    # Create the card output file, based on PID and time.
    if(open(OUT, "> $fn"))
    {
        # Trim'em! =)
        $q{'card'} = &hacknChop($q{'card'}, $MAXCARD);
        $q{'fromname'} = &hacknChop2($q{'fromname'}, $MAXFN);
        $q{'fromaddr'} = &hacknChop2($q{'fromaddr'}, $MAXFA);
        $q{'toname'} = &hacknChop2($q{'toname'}, $MAXTN);
        $q{'toaddr'} = &hacknChop2($q{'toaddr'}, $MAXTA);
        $q{'message'} = &hacknChop($q{'message'}, $MAXMESS);

        print OUT "message\t". $q{'message'}."\n";
        print OUT "toaddr\t".  $q{'toaddr'}."\n";
        print OUT "toname\t".  $q{'toname'}."\n";
        print OUT "fromaddr\t".$q{'fromaddr'}."\n";
        print OUT "fromname\t".$q{'fromname'}."\n";
        print OUT "time\t".    (time)."\n";
        print OUT "card\t".    $q{'card'}."\n";
        print OUT "stamp\t".   $q{'stamp'}."\n";
        close(OUT);
        
        $mess = "Dear $q{'toname'},\n\n   You've been sent an NHS50 postcard by $q{'fromname'} ($q{'fromaddr'}).  Please go to:\n\n\t$DISPLAY_URL?id=$id\n\n  to collect it!\n\nNHS 50 Postman,\npostman\@nhs50.nhs.uk\n\n--\nVisit the NHS50 website: http://www.nhs50.nhs.uk/\n";
        $ret = &sender("NHS50 Postman <postman\@nhs50.nhs.uk>", $q{'toaddr'}, "You've been sent an NHS50 Postcard!", $mess);
        if($ret == 0)
        {
            $trans{'code'} = "Your card has been sent!";
        }
        else
        {
            $trans{'code'} = "Your card has not been sent! (error code $ret)";
        }
    }
    else
    {
        $trans{'code'} = "Your card has not been sent! (error code NO)";
    }

    &saveSettings(\%trans);
    &modge($template, \%trans);
    
}

################################################################################
# cardBlowup: displays a blown-up version of the image
sub cardBlowup
{
    my $template = &loadFile($TEMPLATES."postcard-blowup.htm");
    my %trans;

    &saveSettings(\%trans);

    $trans{'img'} = '<center><p align="center"><img src="'.$IMGURL.$q{'card'}.'d.jpg"></p></center>';
    $trans{'credits'} = &loadFile($IMAGES.$q{'card'}."cred.txt")."<br>";

    &modge($template, \%trans);
}

################################################################################
# getBlowup: get the blown-up image number out of the parameter strings.
# a blowup is indicated by stage_blowup123.x being set, if 123 is the image to 
# blow up.
sub getBlowup
{
    my $r;
    foreach $r (%q)
    {
        if($r =~ /^stage_blowup([0-9]+)/)
        {
            return $1;
        }
    }

    return 0;
}

################################################################################
# imgList: return a list of the image codes that are present and accounted for
sub imgList
{
    if(opendir(DIR, $IMAGES))
    {
        my @files = map { s/b\.jpg$//; $_; } grep /b\.jpg$/, readdir DIR;
        closedir DIR;

        return @files;
    }
    
    return undef;
}

################################################################################
# stampList: return a list of the image codes that are present and accounted for
sub stampList
{
    if(opendir(DIR, $IMAGES."stamps/"))
    {
        my @files = map { s/t\.gif$//; $_; } grep /t\.gif$/, readdir DIR;
        closedir DIR;

        return @files;
    }
    
    return undef;
}

################################################################################
# cardSelect: produce the card selection screen
sub cardSelect
{
    my $default;
    my $template = &loadFile($TEMPLATES."postcard-select.htm");
    my %trans;
    my @pics = sort &imgList;

    # Get a card to start with.  If none specified, use '001'
    $default = ($q{'card'})?$q{'card'}:'001';
    

    my $sel = "<table width=\"100%\">";
    my $count = 0;
    foreach $card (@pics)
    {
        # Quite ghastly.  It works, though.
        $sel .= "\n<tr>" if(($count % 3) == 0);
        my $radio = "<input type=radio name=card value=\"$card\"";
        $radio .= " checked" if($card == $default);
        $radio .= ">";
        $sel .= "<td align=\"center\" width=\"33%\" valign=\"middle\">";
        $sel .= "<input type=image src=\"".$IMGURL.$card."t.jpg\" name=\"stage_blowup$card\" border=\"1\"><br>";
        $sel .= "$radio<br><br></td>\n";
        $sel .= "</tr>" if(($count % 3) == 2);

        $count++;
    }
    $sel .= "</tr>" if((($count-1)%3)!=2);
    $sel .= "</table>\n";
    $trans{'selector'} = $sel;

    # Save previously set settings in wizard environment
    &saveSettings(\%trans);
    &modge($template, \%trans);
}

################################################################################
# cardCompose: produces the card composition page
sub cardCompose
{
    my $template = &loadFile($TEMPLATES."postcard-compose.htm");
    my %trans;

    &saveSettings(\%trans);

    &modge($template, \%trans);
}

################################################################################
# cardChecksettings: returns 0 and prints error page if card details are bad.
# otherwise quietly returns 1;
sub cardChecksettings
{
    my $error = "";
    
    $error .= "<ul><li><em>Your message is too long ($MAXMESS characters max)</em></li></ul>" if(length($q{'message'})>$MAXMESS);
    $error .= "<ul><li><em>'Their E-mail' is too long ($MAXTA characters max)</em></li></ul>" if(length($q{'toaddr'})>$MAXTA);
    $error .= "<ul><li><em>'Their Name' is too long ($MAXTN characters max)</em></li></ul>" if(length($q{'toname'})>$MAXTN);
    $error .= "<ul><li><em>'Your E-mail' is too long ($MAXFA characters max)</em></li></ul>" if(length($q{'fromaddr'})>$MAXFA);
    $error .= "<ul><li><em>'Your Name' is too long ($MAXFN characters max)</em></li></ul>" if(length($q{'fromname'})>$MAXFN);
    $error .= "<ul><li><em>'Their Name' is not valid</em></li></ul>" if($q{'toname'} !~ /\w+/);
    $error .= "<ul><li><em>'Your Name' is not valid</em></li></ul>" if($q{'fromname'} !~ /\w+/);
    $error .= "<ul><li><em>'Their E-mail' is not a valid e-mail address</em></li></ul>" if($q{'toaddr'} !~ /^[\w\.][\w\.\-+]*@[a-z0-9\.\-]+$/);
    $error .= "<ul><li><em>'Your E-mail' is not a valid e-mail address</em></li></ul>" if($q{'fromaddr'} !~ /^[\w\.][\w\.\-+]*@[a-z0-9\.\-]+$/);
    $error .= "<ul><li><em>Your message is not valid</em></li></ul>" if($q{'message'} !~ /\w+/);

    if($error ne "")
    {
        $error = "The following problems were found with your message:\n".$error;
        
        my $template = &loadFile($TEMPLATES."postcard-error.htm");
        my %trans;
        
        $trans{'error'} = $error;

        &saveSettings(\%trans);
        &modge($template, \%trans);
        return 0;
    }
    
    return 1;
}    

################################################################################
# saveSettings: copy standard settings into template translation table.
sub saveSettings
{
    my ($trans) = @_;

    $$trans{'MAXMESS'} = $MAXMESS;
    $$trans{'MAXTA'} = $MAXTA;
    $$trans{'MAXTN'} = $MAXTN;
    $$trans{'MAXFA'} = $MAXFA;
    $$trans{'MAXFN'} = $MAXFN;
    $$trans{'MAXCARD'} = $MAXCARD;

    $$trans{'thumb'} = "";
    $$trans{'card'} = $q{'card'};
    $$trans{'toaddr'} = $q{'toaddr'};
    $$trans{'toname'} = $q{'toname'};
    $$trans{'fromaddr'} = $q{'fromaddr'};
    $$trans{'fromname'} = $q{'fromname'};
    $$trans{'message'} = $q{'message'};
    $$trans{'MAINURL'} = $MAINURL;
    $$trans{'url'} = $ME;
}

################################################################################
# cardStamp: produces stamp selection page.
sub cardStamp
{
    my $template = &loadFile($TEMPLATES."postcard-stamp.htm");
    my %trans;
    my @pics = sort &stampList;

    my $sel = "<table width=\"100%\">";
    my $count = 0;

    my $default = ($q{'stamp'})?$q{'stamp'}:"1";
    
    foreach $stamp (@pics)
    {
        # Quite ghastly.  It works, though.
        $sel .= "\n<tr>" if(($count % 3) == 0);
        my $radio = "<input type=radio name=stamp value=\"$stamp\"";
        $radio .= " checked" if($stamp == $default);
        $radio .= ">";
        $sel .= "<td align=\"center\" width=\"33%\" valign=\"middle\">";
        $sel .= "<img src=\"".$IMGURL."stamps/".$stamp."t.gif\" border=0><br>";
        $sel .= "$radio<br><br></td>\n";
        $sel .= "</tr>" if(($count % 3) == 2);

        $count++;
    }
    $sel .= "</tr>" if((($count-1)%3)!=2);
    $sel .= "</table>\n";
    $trans{'stamp'} = $sel;


    &saveSettings(\%trans);
    
    &modge($template, \%trans);
}

################################################################################
# modge: fills in the blanks, and prints to stdout. 
sub modge
{
    my ($template, $trans) = @_;
    
    my $key;
    
    # searches for <<blah>> and replaces with $$trans{'blah'}.
    $template =~ s/\<\<(\w+)\>\>/$$trans{$1}/g;
    print $template;
}

################################################################################

sub loadFile
{
    my ($fn) = @_;
    my $temp = "Not found";

    if((-f $fn) && open(TEMPLATE, $fn))
    {
        $temp = "";
        while(<TEMPLATE>)
        {
            $temp .= $_;
        }
        close(TEMPLATE);
    }

    return $temp;
}
