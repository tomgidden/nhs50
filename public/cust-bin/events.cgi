#!/usr/bin/perl --

require 5.001;
close STDERR;
$SIG{HUP} = sub { &unlock; exit; };

$starttime = (times)[0];

require "./cgi-lib.pl";
### The CGI script "events.cgi" is used to produce user-specific search ###
### results from the NHS Events Database (a flat # delimited text file).###
### Author: Tom Gidden 													###
### Contact Details: 													###
### 		Telephone (w): 0117 914 6257 								###
###         Email:         tgidden@brann-interactive.com				###


# Virtual directories of the text files and html pages referenced from this script
# These directories must be changed to correspond to the BT WebWorld directories
$HOME = "/tmp/nhs50_events";
$EVENTS_FILE = "$HOME/datafile.txt";
$START_FILE = "$HOME/start.htm";
$END_FILE = "$HOME/end.htm";
$EDITFORM_FILE = "$HOME/editform.htm";
$RECORD_FILE = "$HOME/template.htm";

# These should give a good idea of the operation of the date and time parsing system:
#print &interpretDates("980101-980201")."\n";
#print &interpretDates("980101-")."\n";
#print &interpretDates("-980201")."\n";
#print &interpretDates("980101-980101")."\n";
#print &interpretDates("980101,980201")."\n";
#print &interpretDates("980101-980201,980301-980401")."\n";
#print &interpretDates("980101,980201,980301")."\n";
#print &interpretTimes("1200-1400,1900-2000,0000-0600")."\n";

################################################################################

sub todayDate
# Gets date.. this entire CGI is _not_ Year 2000 compliant! =)
{
    my (@time, $today);

    @time = gmtime(time);
    
    $today = 
        substr("0000".$time[5], -2).          # Year
            substr("0000".($time[4]+1), -2).          # Month
                substr("0000".$time[3], -2);          # Day
    
    return $today;
}

################################################################################

sub rewriteDate
# Rewrites a date from YYMMDD to Month, Day, Year.
{
    my ($codedate) = pop(@_);

    $year = substr($codedate, 0, 2)+1900;
    $month = (qw(January February March April May June July August September October November December))[substr($codedate, 2, 2) - 1];
    $day = substr($codedate, 4, 2)+0;

    return "$month $day, $year";
}

################################################################################

sub interpretTimes
# Rewrites the time into AM and PM format
{
    my ($times) = pop(@_);

    while($times =~ s/(\d\d)(\d\d)/FOUND/)
    {
        my $h = $1+0;
        my $m = substr("0000".$2, -2);
        my $ampm = "am";
        my $nicetime;
        
        if($h > 11)
        {
            $ampm = "pm";
        }
        if($h > 12)
        {
            $h -=12;
        }
        elsif($h == 0)
        {
            $h = 12;
        }
        
        $nicetime = "$h.$m $ampm";

        $times =~ s/FOUND/$nicetime/g;
    }
    
    $times =~ s/\,/\, /g;  # Tidy punctuation
    $times =~ s/\-/ \- /g; # Tidy punctuation
    return $times;
}

################################################################################

sub testMonthRange
{
    my ($range, $test) = @_;
    my $ret = 0;
    $test = $test+0;
    my $against;

    foreach $against (split(/\,/, $range))
    {
        if($against =~ /^(\d\d\d\d\d\d)\-(\d\d\d\d\d\d)$/)
        {
            my $from = substr($1, 0, 4)+0;
            my $to = substr($2, 0, 4)+0;
            
            $ret = 1 if(($test>=$from) && ($test<=$to));
        }
        elsif($against =~ /^(\d\d\d\d\d\d)\-$/)
        {
            my $from = substr($1, 0, 4)+0;
            
            $ret = 1 if($test>=$from);
        }
        elsif($against =~ /^\-(\d\d\d\d\d\d)$/)
        {
            my $to = substr($1, 0, 4)+0;

            $ret = 1 if($test<=$to);
        }
        elsif($against =~ /^(\d\d\d\d\d\d)$/)
        {
            my $from = substr($1, 0, 4)+0;
            $ret = 1 if($from == $test);
        }
    }

    return $ret;
}

################################################################################

sub interpretDates
# Horribly complex date interpreter!
# This interprets a variety of date formats which may used in the data file.
{
    my ($dates) = pop(@_);

    @indivs = split(/\,/, $dates);

    my $nicedate = "";
    my $lastdate = &todayDate+10;
    
    for($indivCount = 0; $indivCount<=$#indivs; $indivCount++)
    {
        my $indiv = $indivs[$indivCount];

        if ($indiv =~ /^(\d\d\d\d\d\d)$/) # Single date, YYMMDD
        {
            my $startDate = $1;
            $nicedate.= &rewriteDate($startDate);
            $lastdate = $startDate;
        }
        elsif ($indiv =~ /^(\d\d\d\d\d\d)-(\d\d\d\d\d\d)$/) # YYMMDD-YYMMDD: Range
        {
            my $startDate = $1;
            my $endDate = $2;
            
            if ($startDate eq $endDate)
            {
                $nicedate.= &rewriteDate($startDate);
                $lastdate = $startDate;
            }
            else
            {
                $nicedate.= "From ".&rewriteDate($startDate)." until ".&rewriteDate($endDate);
                $lastdate = $endDate;
            }
        }
        elsif ($indiv =~ /^(\d\d\d\d\d\d)-$/)   # YYMMDD-:  From
        {
            my $startDate = $1;
            
            $nicedate.= "From ".&rewriteDate($startDate);
        }
        elsif ($indiv =~ /^-(\d\d\d\d\d\d)$/)   # -YYMMDD:  Until
        {
            my $endDate = $1;
            
            $nicedate.= "Until ".&rewriteDate($endDate);
            $lastdate = $endDate;
        }
        else
        {
            $nicedate = "N/A";
        }
        if($indivCount < $#indivs) { $nicedate .="; "; } # Add semi-colon to list
    }

    if (($lastdate+0)<(&todayDate+0)) { $nicedate = "($nicedate)"; } # Invalidate past events with brackets

    return $nicedate;
} 

################################################################################

sub setupRecordLoader
{
    

}

################################################################################

sub process
# The main events matching loop.   One pass!
{
    my (%cgiparams, $url) = @_;

    my $line = 0;
    my $firstrecord = $cgiparams{"firstrecord"};
    my $batch = $cgiparams{"batch"};
    my $matches = 0;
    
    # Print HTML Header
    print &smartReplace($START_STRING, %cgiparams);

    # Load events file.
    if(open(EVENTS, $EVENTS_FILE))
    {
        while(<EVENTS>)
        {
            if ($line == 0)
            {
                @colnames = split(/\#/);
            }
            else
            {
                my (%record, @data, $datum);
                
                @data = split(/\#/);

                for($datum = 0; $datum < $#data; $datum++)
                {
                    $record{$colnames[$datum]} = $data[$datum];
                }
                
                $record{"Approved"} = "0" if($record{"Approved"} ne "1");
                $record{"Times"} = &interpretTimes($record{"TimeOfEvent"});
                $record{"Dates"} = &interpretDates($record{"DateOfEvent"});

                my $bool = 1;  # Innocent-until-proven-guilty matching, i.e. stays '1' while it still matches
                my $searchterm;

                foreach $searchterm (keys %cgiparams)
                {
                    my $val = $cgiparams{$searchterm};
                    
                    if(substr($searchterm, 0, 1) =~ /[A-Z]/) # Capital letters to start search terms
                    {
                        if($searchterm eq "Dates")
                        {
                            $bool = $bool && ((length($cgiparams{$searchterm})<2) || &testMonthRange($record{"DateOfEvent"}, $cgiparams{$searchterm}));
                        }
                        elsif($searchterm eq "Keywords")
                        {
                            my $key;
                            my $bool2;
                            $bool2 = 0;
                            
                            foreach $key (grep /^[A-Z]/, keys %record)
                            {
                                $bool2 = 1 if ($record{$key} =~ /$val/i);
                            }
                            
                            $bool = 0 unless ($bool2 == 1);
                        }
                        else
                        {
                            $bool = 0 unless (($record{$searchterm} =~ /$val/i) || length($val)<1);
                        }
                    }
                }

                
                if($record{"Approved"} eq "1")
                {
                    $record{"approvedcol"} = '"#ffcc00"';
                }
                else
                {
                    if ($cgiparams{"editor"} eq "yes")
                    {
                        $record{"approvedcol"} = '"#ff0000"';
                    }                        
                    else
                    {
                        $bool = 0;
                    }
                }
                
                if ($cgiparams{"editor"} eq "yes")
                {
                    $record{"editmebutton"} = 
                        '<a href="'.&newURL( ( 'editor'=>'yes', 'edit'=>'yes', 'IdNum' => $record{"IdNum"} ), $url).'">EDIT</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.
                            '<a href="'.&newURL( ( 'editor'=>'yes', 'delete'=>'yes', 'submitted'=>'yes', 'IdNum' => $record{"IdNum"} ), $url).'">DELETE</a> ';
                    $record{"editor"} = "yes";
                }
                else
                {
                    $record{"editmebutton"} = '';

                    $bool = 0 if($record{"Dates"} =~ /\(/);
                }


                if ($bool != 0)
                {
                    $matches ++;
                    
                    if(($matches>=$firstrecord) && ($matches<$firstrecord+$batch))
                    {
                        print &smartReplace($RECORD_STRING, %record);
                    }
                }
            }
            
            
            $line ++;
        }

        my %additional = %cgiparams;

        if($matches > ($firstrecord + $batch))
        {
            # More records...inserts a next button
            $additional{"firstrecord"} = $firstrecord+$batch;
            $nextbutton = $cgiparams{"nextbutton"} = &newURL(%additional, $url);
        }

        if($firstrecord > 0)
        {
            # Previous records...inserts a previous button
            $additional{"firstrecord"} = (($firstrecord-$batch)<0)?0:$firstrecord-$batch;
            $prevbutton = $cgiparams{"prevbutton"} = &newURL(%additional, $url);
        }


        if($matches < 1) 
        {
            $cgiparams{"STATE"} = "No matches were found. Please try searching again, with a wider search:";
        }
        else
        {
            $cgiparams{"STATE"} = "";
        }

        close EVENTS;
    }
    else
    {
        $cgiparams{"STATE"} = "There has been an internal error.  Please try again in a few minutes.";
    }

    $cgiparams{'MATCHES'} = $matches;
    
    print &smartReplace($END_STRING, %cgiparams);
}

################################################################################

sub newURL
# Generates a new URL, based on the old, but with the new param set
{
    my (%params, $url) = @_;

    my $new_querystring = "";
    
    foreach $key (keys %params)
    {
        $new_querystring .= "$key=".$params{$key}."&";
    }
    chop $new_querystring; # Remove trailing &

    return $url."?".$new_querystring;
}

################################################################################

sub loadHTMLFiles
# Load HTML Nuggets into global variables
{
    if(open(LOAD, $START_FILE)) { $START_STRING = ""; while (<LOAD>) { $START_STRING.=$_; } close (LOAD); } else { $START_STRING="Error"; }
    if(open(LOAD, $RECORD_FILE)) { $RECORD_STRING = ""; while (<LOAD>) { $RECORD_STRING.=$_; } close (LOAD); } else { $RECORD_STRING="Error"; }
    if(open(LOAD, $END_FILE)) { $END_STRING = ""; while (<LOAD>) { $END_STRING.=$_; } close (LOAD); } else { $END_STRING="Error"; }
    if(open(LOAD, $EDITFORM_FILE)) { $EDITFORM_STRING = ""; while (<LOAD>) { $EDITFORM_STRING.=$_; } close (LOAD); } else { $EDITFORM_STRING="Error"; }
}

################################################################################

sub smartReplace
# Use the hash table for a smart search and replace.
{
    my ($line, %record) = @_;
    
    while(0
          || ($line =~ s/\<(if)\s*\[(.*?)\]\>(.*?)\<else\>(.*?)\<\/if\>/REPLACEMENOW/is)      # <if [1==1]>x<else>y</if> is replaced by x
          || ($line =~ s/\<(insert)\s+(\/?[a-zA-Z0-9\-]+)\s*\>/REPLACEMENOW/si)               # <insert x> is replaced by $record{"x"}
          )
    {
        $tag = $1;    # Either 'if' or 'insert' 
        $key = $2;
        $inner1 = $3;
        $inner2 = $4;
        
        if($tag eq "if")
        {
            # Check condition
            if (eval($key))
            {
                $val = $inner1;
            }
            else
            {
                $val = $inner2;
            }
            
            $line =~ s/REPLACEMENOW/$val/s;
        }
        elsif($tag eq "insert")
        {
            $val = $record{$key};
            $line =~ s/REPLACEMENOW/$val/s;
        }
    }

    return $line;
}

################################################################################

sub prepareEntryForm
{
    my (%cgiparams) = @_;
    
    &sharedLock;

    print &smartReplace($START_STRING, %cgiparams);

    if(open(EVENTS, $EVENTS_FILE))
    {
        $line = 0;
        $found = 0;

        while(<EVENTS>)
        {
            if ($line == 0)
            {
                @colnames = split(/\#/);
            }
            else
            {
                my (%record, @data, $datum);
                
                @data = split(/\#/);

                for($datum = 0; $datum < $#data; $datum++)
                {
                    $record{$colnames[$datum]} = $data[$datum];
                }
                
                if($record{"IdNum"} eq $cgiparams{"IdNum"})
                {
                    $record{"Times"} = &interpretTimes($record{"TimeOfEvent"});
                    $record{"Dates"} = &interpretDates($record{"DateOfEvent"});

                    $record{"TimeOfEvent"} =~ /^(\d\d\d\d)?-?(\d\d\d\d)?$/;
                    $record{"ToHour"} = substr($2, 0, 2);
                    $record{"ToMin"} = substr($2, 2, 2);
                    $record{"FromHour"} = substr($1, 0, 2);
                    $record{"FromMin"} = substr($1, 2, 2);

                    $record{"DateOfEvent"} =~ /^(\d\d\d\d\d\d)?-?(\d\d\d\d\d\d)?$/;
                    $record{"ToDay"} = substr($2, 4, 2);
                    $record{"ToMonth"} = substr($2, 2, 2);
                    $record{"FromDay"} = substr($1, 4, 2);
                    $record{"FromMonth"} = substr($1, 2, 2);
                    $record{"editor"} = $cgiparams{"editor"};
                    $found = 1;

                    &generateEntryForm(%record);
                }
            }
            
            $line++;
        }
        
        if ($found == 0) { &generateEntryForm(editor=>$cgiparams{"editor"}); }

        close EVENTS;
    }


}

################################################################################

sub generateEntryForm
{
    my (%params) = @_;
    my (%record);

    %regions = (
                '00UK' => 'Unknown',
                AO => 'Anglia & Oxford',
                NI => 'Northern Ireland',
                NT => 'North Thames',
                NW => 'North West',
                NY => 'Northern & Yorkshire',
                ST => 'South Thames',
                SW => 'South West',
                TR => 'Trent',
                WM => 'West Midlands',
                WA => 'Wales',
                SC => 'Scotland'
                );
    
    %types = (
              '00UNKNOWN' => "Please select a type",
              'OPEN DAY' => "Open Day",
              'FETE' => "Fete",
              'EXHIBITION' => "Exhibition",
              'SOCIAL (STAFF)' => "Social (staff)",
              'SCHOOLS EVENTS' => "Schools Events",
              'PRESENTATION' => "Presentation",
              'OFFICIAL OPENING' => "Official Opening",
              'XXOTHER' => "Other"
              );
    

    $record{"RegionInput"} = &generatePulldownInput(\%params,
                                                    \%regions,
                                                    "Region");

    $record{"TypeInput"} = &generatePulldownInput(\%params,
                                                  \%types,
                                                  "Type");

    $record{"FromDateInput"} = &generateDateInput(%params, "From");
    $record{"ToDateInput"} = &generateDateInput(%params, "To");

    $record{"FromTimeInput"} = &generateTimeInput(%params, "From");
    $record{"ToTimeInput"} = &generateTimeInput(%params, "To");
    
    $record{"OrganisationNameInput"} = &generateTextField(%params, "OrganisationName");
    $record{"TownInput"} = &generateTextField(%params, "Town");
    $record{"VenueInput"} = &generateTextField(%params, "Venue");
    $record{"PressInput"} = &generateTextField(%params, "Press");
    $record{"PublicInput"} = &generateTextField(%params, "Public");
    $record{"DetailsInput"} = &generateTextField(%params, "Details");

    $record{"CostInput"} = &generateTextField(%params, "Cost");

    $record{"AudienceInput"} = &generateTextField(%params, "Audience");
    $record{"ApprovalByInput"} = &generateTextField(%params, "ApprovalBy");
    $record{"ApprovalDateInput"} = &generateTextField(%params, "ApprovalDate");
    $record{"NotesInput"} = &generateTextField(%params, "Notes");
    $record{"ApprovedInput"} = &generateTickBox(%params, "Approved");
    $record{"editor"} = $params{"editor"};

    $record{"IdNum"} = $params{"IdNum"};

    if($params{"editor"} eq "yes")
    {
        $record{"IdNumInput"} = "<input type=\"hidden\" name=\"IdNum\" value=\"".$params{"IdNum"}."\">" if(($params{"IdNum"}+0)>0);
        $record{"editorInput"} = "<input type=\"hidden\" name=\"editor\" value=\"".$params{"editor"}."\">";
    }

    $record{"submittedInput"} = "<input type=\"hidden\" name=\"submitted\" value=\"yes\">";

    print &smartReplace($EDITFORM_STRING, %record);
}

################################################################################

sub sharedLock
{
    open(LOCK, "> $HOME/lock");
    flock(LOCK, 1);
}

################################################################################

sub exclusiveLock
{
    open(LOCK, "> $HOME/lock");
    flock(LOCK, 2);
}

################################################################################

sub unlock
{
    flock(LOCK, 8);
    close LOCK;
}

################################################################################

sub generateTextField
{
    my (%params, $tag) = @_;
    
    return ("<input type=\"text\" name=\"".$tag."Input\" value=\"".$params{$tag}."\">");
}

################################################################################

sub generateTickBox
{
    my (%params, $tag) = @_;
    
    if($params{$tag} eq "1")
    {
        return ("<input type=\"checkbox\" name=\"".$tag."Input\" value=\"1\" checked>");
    }
    else
    {
        return ("<input type=\"checkbox\" name=\"".$tag."Input\" value=\"1\">");
    }
}

################################################################################

sub generatePulldownInput
{
    my ($p, $i, $tag) = @_;
    
    my %params = %{$p};
    my %inputs = %{$i};
    
    my $regrec = "<select name=\"".$tag."Input\">";
    my $f;
    my @inpkeys = sort keys %inputs;
    
    foreach $f (@inpkeys)
    {
        my $g = $f;
        $g =~ s/^00//;
        $g =~ s/^XX//;

        if(uc($params{$tag}) eq $g)
        {
            $regrec.= "<option value=\"$g\" selected>$inputs{$f}</option>";
        }
        else
        {
            $regrec.= "<option value=\"$g\">$inputs{$f}</option>";
        }
    }

    $regrec.= "</select>";
    return $regrec;
}

################################################################################

sub generateTimeInput
{
    my (%params, $tag) = @_;
    
    my $regrec = "";

    my $f;

    $regrec.= "<select name=\"".$tag."Hour\">";

    foreach $f ('--', '00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23')
    {
        if(uc($params{$tag."Hour"}) eq $f)
        {
            $regrec.= "<option value=\"$f\" selected>$f</option>";
        }
        else
        {
            $regrec.= "<option value=\"$f\">$f</option>";
        }
    }

    $regrec.= "</select>:<select name=\"".$tag."Min\">";

    foreach $f ('--', '00', '05', '10', '15', '20', '25', '30', '35', '40', '45', '50', '55')
    {
        if(uc($params{$tag."Min"}) eq $f)
        {
            $regrec.= "<option value=\"$f\" selected>$f</option>";
        }
        else
        {
            $regrec.= "<option value=\"$f\">$f</option>";
        }
    }

    $regrec.= "</select>";

    return $regrec;
}
################################################################################

sub generateDateInput
{
    my (%params, $tag) = @_;
    
    my $regrec = "";

    my $f;
    my %months = (
                  "--" => '--',
                  "01" => 'January',
                  "02" => 'February',
                  "03" => 'March',
                  "04" => 'April',
                  "05" => 'May',
                  "06" => 'June',
                  "07" => 'July',
                  "08" => 'August',
                  "09" => 'September',
                  "10" => 'October',
                  "11" => 'November',
                  "12" => 'December'
                  );

    $regrec.= "<select name=\"".$tag."Day\">";

    foreach $f ('--', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31')
    {
        if(uc($params{$tag."Day"}) eq $f)
        {
            $regrec.= "<option value=\"$f\" selected>$f</option>";
        }
        else
        {
            $regrec.= "<option value=\"$f\">$f</option>";
        }
    }

    
    $regrec.= "</select> <select name=\"".$tag."Month\">";

    foreach $f (sort keys %months)
    {
        if(uc($params{$tag."Month"}) eq $f)
        {
            $regrec.= "<option value=\"$f\" selected>$months{$f}</option>";
        }
        else
        {
            $regrec.= "<option value=\"$f\">$months{$f}</option>";
        }
    }

    $regrec.= "</select>";

    return $regrec;
}

################################################################################

sub submitEvent
{
    my(%cgiparams) = @_;
    my ($bigid, $line, $thisline, $invalid);

    $line = 0;
    $bigid = 0;
    $invalid = 0;

    # Load events file.
    if(open(EVENTS, $EVENTS_FILE))
    {

        while(<EVENTS>)
        {
            chomp;
            $thisline = $_;

            if ($line == 0)
            {
                @colnames = split(/\#/, $thisline);
            }
            else
            {
                my (%record, @data, $datum);
                
                @data = split(/\#/, $thisline);

                for($datum = 0; $datum < $#data; $datum++)
                {
                    $record{$colnames[$datum]} = $data[$datum];
                }

                unless(($record{"IdNum"} eq $cgiparams{"IdNum"}) && ($cgiparams{"editor"} eq "yes"))
                {
                    push (@database, $thisline);
                }

                $bigid = $record{"IdNum"} if($record{"IdNum"} > $bigid);
            }

            $line++;
        }
        
#        $invalid = 1 if(0 
 #          || (length($cgiparams{"OrganisationNameInput"})<1)
  #         || (length($cgiparams{"Details"})<1));
            

        $cgiparams{"ApprovedInput"} = "0" if ($cgiparams{"ApprovedInput"} eq "");
        # Form processing
        if($cgiparams{"editor"} ne "yes")
        {
            $cgiparams{"ApprovedInput"} = "0";
        }
        else
        {
            $cgiparams{"ApprovalDateInput"} = &todayDate if($ApprovedInput eq "1");
        }

        $cgiparams{"RegionInput"} = lc($cgiparams{"RegionInput"});

        # Contact Process
        my $contact;
        $contact = $cgiparams{"PressInput"};
        $cgiparams{"PressInput"} = $cgiparams{"PublicInput"} if($contact !~ /[0-9A-Za-z]/);
        $cgiparams{"PublicInput"} = $contact if($cgiparams{"PublicInput"} !~ /[0-9A-Za-z]/);

        # New forms get the highest ID number
        if(($cgiparams{"editor"} ne "yes") || (($cgiparams{"IdNum"}+0) < 1))
        {
            $cgiparams{"IdNumInput"} = ($bigid+1);
        }
        else
        {
            $cgiparams{"IdNumInput"} = $cgiparams{"IdNum"};
        }

        
        my ($totime, $fromtime, $todate, $fromdate, @days);
        @days = ( 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 );
        
        # Truncate the day of the month to the specific month's last day
        $cgiparams{"ToDay"} = $days[($cgiparams{"ToMonth"}+0)] if($days[($cgiparams{"ToMonth"}+0)] < ($cgiparams{"ToDay"}+0));
        $cgiparams{"FromDay"} = $days[($cgiparams{"FromMonth"}+0)] if($days[($cgiparams{"FromMonth"}+0)] < ($cgiparams{"FromDay"}+0));

        # Chomp times if bad
        if(($cgiparams{"ToHour"} eq "--") || ($cgiparams{"ToMin"} eq "--"))
        {            $totime = "";        }
        else
        {            $totime = substr($cgiparams{"ToHour"}.$cgiparams{"ToMin"}, 0, 4);        }
        
        if(($cgiparams{"FromHour"} eq "--") || ($cgiparams{"FromMin"} eq "--"))
        {            $fromtime = "";        }
        else
        {            $fromtime = substr($cgiparams{"FromHour"}.$cgiparams{"FromMin"}, 0, 4);        }
        
        if(($cgiparams{"ToDay"} eq "--") || ($cgiparams{"ToMonth"} eq "--"))
        {            $todate = "";        }
        else
        {            $todate = substr("98".$cgiparams{"ToMonth"}.$cgiparams{"ToDay"}, 0, 6);        }
        
        if(($cgiparams{"FromDay"} eq "--") || ($cgiparams{"FromMonth"} eq "--"))
        {            $fromdate = "";        }
        else
        {            $fromdate = substr("98".$cgiparams{"FromMonth"}.$cgiparams{"FromDay"}, 0, 6);        }

        $cgiparams{"TimeOfEventInput"} = $fromtime."-".$totime;
        $cgiparams{"DateOfEventInput"} = $fromdate."-".$todate;
    }
    
    $cgiparams{"SubmissionDateInput"} = &todayDate;
    
    my $f;
    foreach $f (keys %cgiparams)
    {
        $cgiparams{$f} =~ s/\#/�/g;
        $cgiparams{$f} =~ s/\</\&lt\;/g;
        $cgiparams{$f} =~ s/\>/\&gt\;/g;
        $cgiparams{$f} =~ s/\n/ /g;
        $cgiparams{$f} =~ s/\r//g;
        $cgiparams{$f} =~ s/\s+/ /g;
    }

    my $newrecord = "";
    my $colheads = "";

    foreach $f (@colnames)
    {
        $newrecord.= substr($cgiparams{$f."Input"}."#", 0, 255);
        $colheads.= "$f#";
    }

    push @database, $newrecord unless (($cgiparams{"delete"} eq "yes") && ($cgiparams{"editor"} eq "yes"));

    if($invalid == 0)
    {
        # Write out the database file
        open(OUT, "> $EVENTS_FILE");
        print OUT $colheads."\n";
        foreach $f (@database)
        {
            chomp $f;
            print OUT $f."\n";
        }
        close(OUT);
    }

    # Display events
    my %additional;


    foreach $f (keys %cgiparams)
    {
        my $g;
        $g = $f;
        $g =~ s/Input$//g;

        $additional{$g} = $cgiparams{$f};
    }
    $additional{"Times"} = &interpretTimes($additional{"TimeOfEvent"});
    $additional{"Dates"} = &interpretDates($additional{"DateOfEvent"});        

    
    $additional{"STATE"} = 'Your event has been submitted.  It will be checked by the site editor, and made live as soon as possible.  A proof of your event is listed above. If you have any queries or alterations to make, please e-mail <a href="mailto:editor@nhs50.nhs.uk">editor@nhs50.nhs.uk</a>, quoting reference <em>'.$additional{"IdNum"}.'</em>';
    $additional{"approvedcol"} = "#ffcc00";

    print &smartReplace($START_STRING, %additional);
    print &smartReplace($RECORD_STRING, %additional);
    print &smartReplace($END_STRING, %additional);
}

################################################################################

sub byDateField
{
    @dfa = split(/\#/, $a);
    @dfb = split(/\#/, $b);
    $dfa = $dfa[2];
    $dfb = $dfb[2];

    if($dfa =~ /^(\d\d\d\d\d\d)?-?(\d\d\d\d\d\d)?$/)
    {
        if($1 > 0) { $dfa = $1; } else { $dfa = &todayDate; }
    }
    else
    {
        $dfa = &todayDate;
    }

    if($dfb =~ /^(\d\d\d\d\d\d)?-?(\d\d\d\d\d\d)?$/)
    {
        if($1 > 0) { $dfb = $1; } else { $dfb = &todayDate; }
    }
    else
    {
        $dfb = &todayDate;
    }
    
    $dfa <=> $dfb;
}

################################################################################

MAIN: 
{
    my (%input);  
    
    print "Content-type: text/html\n\n";

    &ReadParse(\%input);
    &loadHTMLFiles;

    if($input{"start"} eq "yes")
    {
    }
    elsif($input{"edit"} eq "yes")
    {
        &sharedLock;
        &prepareEntryForm(%input);
    }
    elsif($input{"submitted"} eq "yes")
    {
        &exclusiveLock;
        &submitEvent(%input);
    }
    elsif($input{"batch"} ne "")
    {
        &sharedLock;
        &process(%input, "events.cgi");
    }
    else
    {
        my %record;
        my $f;

        $record{"STATE"} = '<font face="Arial, Helvetica, Sans serif" size="2" color="#000000"><strong><img src="/gfx/46298525f5cefafa83ac29f77ab420ac.gif" border="0" hspace=0 vspace=0 width="278" height="18" alt="Events Database"></strong></font><br><br></b>Search our massive database of NHS 50th anniversary events the easy way.  Enter your search criteria (for example, by town/city/area, by date) and submit the form.  Our search engine will have an answer for you in seconds...<b>';

        foreach $f (keys %input)
        {
            $record{$f} = $input{$f};
        }
        
        print &smartReplace($START_STRING, %record);
        print &smartReplace($END_STRING, %record);
    }

    if($input{"debug"} eq "yes")
    {
        printf "<pre>%.2f CPU Seconds</pre>\n", ((times)[0])-$starttime;
    }

    &unlock;
}

